import matplotlib.pyplot as plt
from pdfs.interference import Interference
import numpy as np

fig, ax = plt.subplots(1, 1)

interference = Interference(Pt=1, ethan=1)

# For plotting purposes
interferencesAxelX = np.linspace(0, 4, 1000)

ax.plot(interferencesAxelX, interference.pdf(interferencesAxelX),

       'r-', lw=5, alpha=0.6, label='interference pdf')

ax.set_title("Interferencias PDF")
ax.set_xlabel("Interferencias(dB)")
ax.set_ylabel("count")
ax.set_label("Interferencias")