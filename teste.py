import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from scipy.stats import rayleigh
import random

fig, ax = plt.subplots(1, 1)

sigmaSquared = np.power(10, -132.0/10)

cellRange = 0.931

gammaTarget = np.power(10,-2.1/10)

D = np.sqrt(3)

# meanPathLossAtCellEdge = np.power(cellRange, -gammaTarget)/(2*np.power(D-cellRange, -gammaTarget) +	
#					2*np.power(D-cellRange, -gammaTarget) + 2*np.power(D, -gammaTarget))

meanPathLossAtCellEdge = np.power(10, (137 - 10*35.2 * np.log10(cellRange))/10)

transmissionPower = [np.power(10, 8.7/10)]*6

loadFactorEtha = []

for i in range(6):
	loadFactorEtha.append(random.random())

lambdan = []

cn = []

interferences = []

interferencesPpfI = 0
interferencesPpfE = 0
interferencesPDF = 0

for i in range(6):
	# Expected value of Rayleigh distribution
	raylieghMean = rayleigh.stats(moments="m")
	
	# Calculate lambdan = ethan*(T/Ln)
	lambdan.append((transmissionPower[i]/meanPathLossAtCellEdge)*loadFactorEtha[i])
	

for i in range(6):
	c = 1
	for j in range(6):
		if(i != j):
			c = (1/(1 - (lambdan[j]/lambdan[i])))*c
	cn.append(c)	

# For plotting purposes
interferencesAxelX = np.linspace(1, 4, 1000)

for i in range(6):
	interferencesPDF = interferencesPDF + ((cn[i]/lambdan[i])/np.exp(-(sigmaSquared*(interferencesAxelX - 1)/lambdan[i])))

interferencesPDF = interferencesPDF*sigmaSquared
ax.plot(10*np.log10(interferencesAxelX), interferencesPDF,

       'r-', lw=5, alpha=0.6, label='Margin interference pdf')

ax.set_title("Margin Interference - PDF")
ax.set_xlabel("Margin Interference(dB)")
ax.set_ylabel("count")
ax.set_label("Interferencias")
plt.show()
