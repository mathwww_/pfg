import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from scipy.stats import rayleigh
import sys
import os


SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from pdfs.mapl import Mapl

fig, ax = plt.subplots(1, 1)

# For plotting purposes
interferencesAxelX = np.linspace(127, 150, 1000)
maplPlot = Mapl()

ax.plot(interferencesAxelX, maplPlot.pdf(interferencesAxelX),

       'r-', lw=5, alpha=0.7, label='MAPL Distribution PDF')

ax.set_title("MAPL Distribution - PDF")
ax.set_xlabel("MAPL(dB)")
ax.set_ylabel("count")
ax.set_label("MAPL")
plt.show()