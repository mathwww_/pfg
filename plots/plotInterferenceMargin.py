import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from scipy.stats import rayleigh
import sys
import os

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from pdfs.interferenceMargin import InterferenceMargin

# fig, ax = plt.subplots(1, 1)

fig = plt.figure(figsize=(10,8))

# For plotting purposes
interferencesAxelX = np.linspace(0, 4, 1000)
im = InterferenceMargin()
im1 = InterferenceMargin(totalPower=100, propagationCoefficient=3.5, loadFactor=0.5, cellRange=3.0, interferenceSources=4)
im2 = InterferenceMargin(totalPower=100, propagationCoefficient=3.5, loadFactor=0.8, cellRange=3.0, interferenceSources=8)
im3 = InterferenceMargin(totalPower=100, propagationCoefficient=3.5, loadFactor=0.4, cellRange=3.0, interferenceSources=8)

plt.plot(interferencesAxelX, im.pdf(interferencesAxelX),'r-', lw=5, alpha=0.6, label='Margin interference pdf, carga=50%')
plt.plot(interferencesAxelX, im1.pdf(interferencesAxelX),'b-', lw=5, alpha=0.6, label='Margin interference pdf, carga=50%')
plt.plot(interferencesAxelX, im2.pdf(interferencesAxelX),'b-.', lw=5, alpha=0.6, label='Margin interference pdf, carga=80%')
plt.plot(interferencesAxelX, im3.pdf(interferencesAxelX),'g+', lw=5, alpha=0.6, label='Margin interference pdf, carga=40%')

plt.title("Margin Interference - PDF")
plt.xlabel("Margin Interference(dB)")
plt.ylabel("count")
plt.show()

