import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from scipy.stats import rayleigh
import sys
import os


SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from pdfs.cellRange import CellRange

fig, ax = plt.subplots(1, 1)

# For plotting purposes
cellRangeAxelX = np.linspace(0.85, 1.15, 1000)
cellRangePlot = CellRange()

ax.plot(cellRangeAxelX, cellRangePlot.pdf(cellRangeAxelX),

       'r-', lw=5, alpha=0.6, label='Cell Range Distribution PDF')

ax.set_title("Cell Range Distribution - PDF")
ax.set_xlabel("Cell Range(m)")
ax.set_ylabel("count")
ax.set_label("Cell Range")
plt.show()