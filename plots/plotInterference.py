import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from scipy.stats import expon, rayleigh

fig, ax = plt.subplots(1, 1)

transmissionPower = [1.0,1.0,1.0,1.0,1.0,1.0]

loadFactorEtha = [1.0,1.0,1.0,1.0,1.0,1.0]

lambdan = []

interferences = []

interferencesPpfI = 0
interferencesPpfE = 0
interferencesPDF = 0

for i in range(6):
	# Expected value of Rayleigh distribution
	raylieghMean = rayleigh.stats(moments="m")
	
	# Calculate lambdan = ethan*(T/Ln)*E[An]
	lambdan.append(transmissionPower[i]*loadFactorEtha[i]*raylieghMean)

	# Creates a RV "interference" with mean 1/lambdan
	interference = expon(loc=0, scale=lambdan[i])

	interferences.append(interference)
	
	# Sum the ppf for each Interferencee RV (needed to generate plot)
	interferencesPpfI = interferencesPpfI + interference.ppf(0.01)
	interferencesPpfE = interferencesPpfE + interference.ppf(0.99)

# For plotting purposes
interferencesAxelX = np.linspace(interferencesPpfI, interferencesPpfE, 1000)

for i in range(6):
	interferencesPDF = interferencesPDF + interferences[i].pdf(interferencesAxelX)

ax.plot(interferencesAxelX, interferencesPDF,

       'r-', lw=5, alpha=0.6, label='interferences pdf')

ax.set_title("Interferencias PDF")
ax.set_xlabel("Interferencias(dB)")
ax.set_ylabel("count")
ax.set_label("Interferencias")
plt.show()
