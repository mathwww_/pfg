import argparse
from scipy.stats import  rayleigh
import numpy as np
from .interferenceMargin import InterferenceMargin
from decimal import Decimal

class Mapl:
    _parse_args = argparse.ArgumentParser().parse_args()

    lMaxDb = 135
    lMax = np.power(10, 13.58)

    def pdf(self, l):
        im = InterferenceMargin()
        xLinear = np.power(10, l/10)
        xArg= np.power(10, (self.lMaxDb-l)/10)
        # maplPDF = im.pdf((self.lMaxDb-x))*(self.lMax/(np.power(linearX, 2)))*((xArg)/(10*np.log10(np.e)))
        # maplPDF = (im.pdf((xArg))/(10*np.log10(np.e)))*(1/(np.power(10, (self.lMaxDb+x)/10)))
        maplPDF = (im.pdf((xArg))/(10*np.log10(np.e)))*np.power(10,(self.lMaxDb-l)/10)

        return maplPDF