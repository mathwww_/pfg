import argparse
from scipy.stats import  rayleigh
import numpy as np
from .mapl import Mapl
from decimal import Decimal

class CellRange:
    _parse_args = argparse.ArgumentParser().parse_args()

    # path loss constant
    KdB = 137
    # path loss exponent
    alphaDb = 15.2

    def pdf(self, x):
        mapl = Mapl()
        linearX = np.power(10, x/10)
        linearK = np.power(10, self.KdB/10)
        # linearAlpha = np.power(10, self.alphaDb/10)
        # linearArgument= np.power(10,((np.power(linearX,linearAlpha))/linearK)/10)
        # randomVariableFromDbToLinear=((np.power(10,((x*self.alphaDb)-self.KdB)/10))/(10*np.log10(np.e)))
        functionArgument= 10*np.log10(linearK/np.power(x,self.alphaDb))
        cellRangePDF = mapl.pdf(functionArgument)*(self.alphaDb/self.KdB)/(np.power(linearX,-1))*functionArgument

        return cellRangePDF