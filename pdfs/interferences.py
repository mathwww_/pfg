import argparse
from scipy.stats import  rayleigh
import numpy as np
from .interferenceMargin import InterferenceMargin
from decimal import Decimal

class CellRange:
    _parse_args = argparse.ArgumentParser().parse_args()

    lMaxDb = 135.8
    lMax = np.power(10, 13.58) 

    def pdf(self, x):
        # x is in dB
        im = InterferenceMargin()
        linearX = np.power(10, x/10)
        print("Lmax: %f" %  self.lMax)
        maplPDF = im.pdf((self.lMaxDb-x))*(np.power(10, self.lMaxDb - (2*x)))*(linearX/(10*np.log10(np.e)))
        
        return maplPDF