import argparse
# interference Probability Density Function
from scipy.stats import  rayleigh
import numpy as np

class InterferenceMargin:

	lambdan = []
	cn = []
	_parse_args = argparse.ArgumentParser().parse_args()

	def __init__(self, totalPower=5.8, interferenceSources=3, loadFactor=0.5, exponentConstant=137,propagationCoefficient=3.5, sigma=-132, cellRange=2.8):
		
		# n is the number number of sectors or number of sites or even amount of interfering cell
		self.interferenceSources=interferenceSources
		# Total Transmission power is given in Watts
		self.transmissionPower = [np.power(10, totalPower/10)/1000]*interferenceSources

		# Load factor of 100% by default
		self.loadFactorEtha = [loadFactor] * interferenceSources

		# Cell Range must be given in Km
		self.D=np.sqrt(3)*cellRange

		self.distance = np.array([self.D/cellRange,(self.D+1)/cellRange,(self.D-1)/cellRange])
	
		# Sigma squared is the total Thermal noise in watts
		self.sigmaSquared= np.power(10, sigma/10)/1000

		self.propagationCoefficient = propagationCoefficient

		self.cellRange=cellRange

	# _parse_args = 0

	def pdf(self, x):

		interferencesPDF = 0

		lambdan=self.lambdaN(propagationCoefficient=3.5,lMax=135,cellRange=3.0,distance=self.distance)
		cn = self.cN(lambdan)
		for i in range(self.interferenceSources):
			if(i!=0):
				interferencesPDF = interferencesPDF + ((cn[i]/self.lambdan[i])*np.exp(-(self.sigmaSquared*(np.power(10, x/10) - 1)/self.lambdan[i]))*np.power(10, x/10)/(10*np.log10(np.e)))

		interferencesPDF = interferencesPDF*self.sigmaSquared
		return interferencesPDF


	def lossPropagationModel(self,propagationCoefficient=2.0, lMax=60.0, cellRange=2.8, distance=np.array([1.0])):
		lossPropagationDB=10*propagationCoefficient*np.log10(distance/cellRange)+lMax
		lossPropagation=np.power([10.0],lossPropagationDB/10.0)

		return lossPropagation,lossPropagationDB
	
	def lambdaN (self,propagationCoefficient=2.0, lMax=135.0, cellRange=1.0, distance=np.array([1.0]), loadFactor=1.0, transmissionPower=40.0):
		lossPropagation, LossPropagation_dB = self.lossPropagationModel(propagationCoefficient,lMax,cellRange,distance)
		lambdan = loadFactor * transmissionPower / lossPropagation
		return lambdan

	def cN (self,lambdan=np.array([1.0])):
		cn = []
		for i in range(len(lambdan)):
			c = 1
			for j in range(len(lambdan)):
				if(i != j and j!= 0 and i != 0):
					c = (1/(1 - (lambdan[j]/lambdan[i])))*c		

			cn.append(c)
		return cn