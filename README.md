```
// or use https://gitlab.com/mathwww_/pfg to download the project
git clone https://gitlab.com/mathwww_/pfg.git
cd pfg
// to view graphic from Interference Margin PDF execute the plot script
python ./plots/plotInterferenceMargin.py
// in windows you must use "\" instead of "/" to navigate through File System
// Interference Margin PDF model is defined at pdf/interferenceMargin.py
```


